.data 
msg: 
.ascii	 "Hello,World!\n"
len = .-msg

.text

.globl_start

_start: 
	mov x0, #2
	ldr x2, = msg
	ldr x3, = len  //equals length of words
	mov w8, #64 //says to write it in the syscall # 64 and means word count
	svc #0 // calls to the system 

// to exit the system call 
	mov x1, #0
	mov w8, #93
	svc #0
