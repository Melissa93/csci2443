.data
msg: 
	.ascii "Favorite Number!"
len = .-msg

.text
.global_start: 

_start: 
	mov x3, #0
loop: 
	add x3, x3, #1
        cmp x3, #3
	beq favnum
	cmp x3, #4
	beq favnum
	cmp x3, #6
	beq favnum
	cmp x3, #10
	beq exit
	b loop


favnum: 
    mov     x0, #1
    ldr     x1, =msg
    ldr     x2, =len
    mov     w8, #64
    svc     #0

b loop //b is for break and the name is loop
B end
end:
mov x8, #93
