.data
array: .word 3,1,5,8,2,4,7,6 //created an array
size: .word 8 //set the size of the array

bubble: .float 2.546 //created a floating point
test:
        .asciz "Is it running?\n\0"
.text
.global main
.extern printf //calls to print the output
main:
        ldr X8, = bubble //loads register with the floating variables
        FMOV D9, X8 //taking what is in X8 and putting it into register D9.
        ldr X8, =array  //load the array
        ldr X9, =size //loads the size of the array
        mov X7, #0 //moves 0 into register 7
reset:
        mov X12, #0 //moves 0 into register 12
loop:
        ldr w1, [X8] //register 8 loads to w1 [32-bit]
        ldr w2, [X8, #4] //moves 4 into register 8 while loading w2 [32-bit]
        add X8,X8,#4 //adds 4 into register 8
        add X7,X7,#1 //points to where in the array we are
        cmp w1,w2 //compares the lowers half the register with x
        ble change // if the branch is less than or equal to the current value
        // ldr X2, [X8, X1, LSL #4] this shifts and loads at the same time
        //LSL X8, X1, #4 calling the shift
        str w2,[X8, #-4] //storing the pre-indexed registers
        // ldr X4, [X8, X1, LSL #4]
        //LSL X8, X1, #4
        str w1,[X8] //storing the pre-indexed registers
        add X12,X12,#1 // add 1 into register 12
        bl change // call for change loop
change:
        cmp X12, C0 // compare register 12
        bgt reset // if the register is leass than or equal to will jump to res>
        cmp X9,X7 // compare registers 9 and 7
        beq Done // if the branch is equal then it will jump to  Done loop

Done:
        ldr X0,=test // load a string of some kind in X0, the "is it running"
        bl printf // prints the output
        mov x8, #93 //exit and return values
        svc 0

